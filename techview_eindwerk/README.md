# TechView

## Setup

1. Install npm
2. Install expo
3. Install Xcode for iOS 
4. Install Android Studio for Android
5. Install a device in Android Studio with Android Virtual Device Manager

## Start Techview

1. Start an Android device with Android Virtual Device Manager
2. Start expo with `npm start` in the project root
3. Press i to run Techview on the iOS Simulator
4. Press a to run Techview on the Android Simulator


## Error? 
FIXING error Invalid regular expression: /(.*\\__fixtures__\\.*|node_modules[\\\]react[\\\]dist[\\\].*|website\\node_modules\\.*|heapCapture\\bundle\.js|.*\\__tests__\\.*)$/:


\node_modules\metro-config\src\defaults\blacklist.js

var sharedBlacklist = [
  /node_modules[/\\]react[/\\]dist[/\\].*/,
  /website\/node_modules\/.*/,
  /heapCapture\/bundle\.js/,
  /.*\/__tests__\/.*/
];

change to:


var sharedBlacklist = [
  /node_modules[\/\\]react[\/\\]dist[\/\\].*/,
  /website\/node_modules\/.*/,
  /heapCapture\/bundle\.js/,
  /.*\/__tests__\/.*/
];



## handleiding 

Home

Te beginnen met het categoriescherm waar de gebruiker kan kiezen tussen welke categorie hij producten zou willen zien.
onderaan heb je 3 mogelijke schermen om naar te navigeren.
-	Home
-	Zoeken
-	Alle producten
Meer uitleg over deze functies bij de volgende afbeeldingen.
De categorieën zijn:
-	Accessoires
-	Beamers
-	Desktops
-	Laptops
-	Monitors
-	Printers
-	Smartphones
-	Tablets
-	 Televisions.


Navigatie opties

 

Linksboven zie je een menu icon, deze verwijst naar het navigatiescherm waar je kan kiezen tussen: 
-	 Home
-	 Filters
-	 Wishlist
-	 Popular
-	 TextMe









Producten
Wanneer je op 1 van de categorieën hebt gedrukt zal je een lijst zien van alle producten die tot deze categorie behoren.
Vervolgens druk je op een product en krijg een detailscherm te zien waar alle belangrijke informatie staat over het product.
Op het detailscherm kan je ook op de icon drukken (hartje) waarna dat product toegevoegd zal worden aan je verlanglijstje

Onderaan op het detailscherm heb je 2 knoppen.  

Comments: zal verwijzen naar het commentscherm waar je reacties kan zien op dat product.
Buy: zal je doorverwijzen naar het specifieke product op Bol.com waar je het vervolgens kan aankopen.




Verlanglijst 

Wanneer je op het hartje gedrukt hebt in het detailscherm  zal je producten zien die je hebt  gekozen om in je verlanglijst op te slaan.
Deze producten blijven in je verlanglijstje staan tot je op 1 van hen drukt en je vervolgens nog eens op het icon (hartje) drukt. 
Nu is het product uit je verlanglijstje.
In figuur 6 staat een overzicht van de verlanglijstje van de toepassing Techview.













Zoeken
Ik geef de gebruiker de optie om producten te zoeken naar eigen input.
De gebruiker zal de mogelijkheid hebben om op alles te zoeken: 
-	Naam
-	Processor
-	Storage
-	Price
-	Software


De gebruiker is niet verplicht om volledige namen te gebruiken, het moment dat de input overeenkomt met de gegevens van het product zal de gebruiker die resultaten zien.
Wanneer je naar de zoek functie navigeert zal ook de onderste navigatie veranderen van kleur.
Dit is omdat deze ook niet telt voor de filteroptie, dit kan je ook zien omdat er linksboven geen icon staat die je naar dat menu zal lijden.
De reden dat ik het zoeken uit mijn filter functionaliteit haal is omdat gebruikers snel hun filters zouden kunnen vergeten waardoor ze zouden kunnen denken dat er geen producten meer zijn of dat de applicatie niet werkt zoals het moet.
In figuur 7,8 staat een overzicht van het zoekscherm van de toepassing Techview.
 

Filter producten & alle producten in een lijst
Te beginnen met het filterscherm waar de gebruiker de keuzen heeft op welke manier hij of zij wil filteren.
Na het kiezen van je filters moet je ook verplicht op de icon (save) duwen zodat deze filter opgeslagen kan worden.
Na deze filters hebben opgeslagen kan je in het homescherm of in producten kijken naar alle producten die meer passen bij wat de gebruiker specifiek wil hebben. Ook zal je de minimum prijs en maximum prijs zien wanneer je door alle producten navigeert. 
In figuur 9 staat een overzicht van het filterscherm van de toepassing Techview.
In figuur 10 staat een overzicht van alle producten van de toepassing Techview.




	
Comments

Dit scherm geeft alle comments weer die op het specifieke product geplaats zijn.
Deze comments hebben een naam en datum van creatie.
De comments zullen ook altijd een default afbeelding hebben, dit is omdat er nog geen mogelijkheid is om aan te melden op mijn applicatie en dus er geen profielfoto kan gegeneerd worden. 








Contact

De gebruiker heeft die optie om mij een sms te versturen met een aanvraag voor een nieuw product in mijn applicatie.
Het is een concept is simpel, je druk op de “SEND” 
knop die vervolgens je zal doorverwijzen naar je 
eigen sms applicatie in je gsm.
Het nummer naar waar je het voorstel gaat
versturen zal automatisch ingevuld zijn voor de gebruiker. 

Er wordt ook gebruik gemaakt van een GIF
Om de openruimte op mijn scherm te verminderen. 




Populaire producten

De gebruiker kan ook zien welke producten veel verkocht worden en/of producten waar veel vraag naar is.
Ook in deze optie kan de gebruiker navigeren naar de details van elk product met hun bijhorende functionaliteiten. 
De populaire producten zullen een gouden rand rond hun hebben, dit moet als aanduiding zijn dat deze speciale producten zijn die veel gevraagd worden en/of veel verkocht worden.








