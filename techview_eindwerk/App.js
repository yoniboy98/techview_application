import React, { useState } from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { useScreens } from 'react-native-screens';
import MainNavigator from './navigation/ProNavigator';
import productReducer from './store/reducers/products';
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import { AsyncStorage } from 'react-native';
import {ScreenOrientation} from 'expo';
ScreenOrientation.unlockAsync()

/** For better performance on native devices */
useScreens();

/**AsyncStorage will save my redux states.
 * When an user close the application, the state will still be the same.  */
const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}



/** Create reducer.*/
const rootReducer = combineReducers({
  products: productReducer
});

const Reducer = persistReducer(persistConfig, rootReducer);


/** Create store (redux) so I can change the state of each component at one time. */
export const store = createStore(Reducer);
export const persistor = persistStore(store);







/** Apply all fonts that I will use in this app.*/
const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),


  });
};

function App() {
  const [fontLoaded, setFontLoaded] = useState(false);

  /*
  const [dbSetup, setDbSetup] = useState(false);
 let db = getDB();

    useEffect(() => {
        (async () => {
            if(!dbSetup){
                await initDb(db);
                setDbSetup(true);
            }
        })();
    })
*/


  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
      />
    );
  }
 /**Root component of my application. */
  return (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
      <MainNavigator />
        </PersistGate>
    </Provider>
  );

}

export default App;
