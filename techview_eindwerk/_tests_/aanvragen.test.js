import * as Queries from "../func/Aanvragen";
/*
const API = "http://81.164.166.118/techviewApi_war_exploded/api/";

beforeEach(() => {
    fetchMock.resetMocks();
});

*/
/*
test("findCommentsByProductId to call API", async () => {
    fetch.mockResponseOnce(JSON.stringify({productId : p1}));

    const comment = await Queries.findCommentByproductId(p1);

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(API + "comment/productId?productId=p1");
});
**/

import { getTextDate } from "../func/Aanvragen";

test('Date "2020-02-01T23:00:00.000+00:00" to return 1 February 2020', () => {
    expect(getTextDate('2020-01-01T00:00:00.000+00:00')).toBe('01 Feb 2020');
});