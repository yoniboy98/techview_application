import React from 'react'



test('Categories render as expected', () => {
    expect('Accessoires, Beamers, Desktops, Laptops, Monitors, Printers, Smartphones, Tablets, Televisions').toMatch(/Accessoires, Beamers, Desktops, Laptops, Monitors, Printers, Smartphones, Tablets, Televisions/);
});


const CategoryList = require("../assets/CATEGORY");


test('Category index 1 JSON render as expected.', () => {
    expect(CategoryList[1]).toStrictEqual({"id": "c9", "photoLink": "https://elektrozine.be/wp-content/uploads/2014/12/EH-TW550.jpg", "title": "Beamers"});
});

test('Category index 2 JSON render as expected.', () => {
    expect(CategoryList[2]).toStrictEqual({"id": "c3", "photoLink": "https://hothardware.com/Image/Resize/?width=1170&height=1170&imageFile=/contentimages/Article/2924/content/big_optiplex-hero-image.jpg", "title": "Desktops"});
});

test('Category index 3 JSON render as expected.', () => {
    expect(CategoryList[3]).toStrictEqual({"id": "c2", "photoLink": "https://top10bestlaptops.com/wp-content/uploads/2019/11/Best-Small-Laptops-768x367.jpg", "title": "Laptops"});
});

test('Category index 4 JSON render as expected.', () => {
    expect(CategoryList[4]).toStrictEqual({"id": "c1", "photoLink": "https://elektrozine.be/wp-content/uploads/2019/07/3683-d5f42e0b3d0d47d080ea3252d73c7d7a.png", "title": "Monitors"});
});

test('Category index 5 JSON render as expected.', () => {
    expect(CategoryList[5]).toStrictEqual({"id": "c6", "photoLink": "https://media.itpro.co.uk/image/upload/t_content-image-desktop@1/v1570815948/itpro/2016/09/secure-printers.jpg", "title": "Printers"});
});

test('Category index 6 JSON render as expected.', () => {
    expect(CategoryList[6]).toStrictEqual({"id": "c4", "photoLink": "https://nl.letsgodigital.org/uploads/2019/01/samsung_opvouwbare-smartphones.jpg", "title": "Smartphones"});
});


test('Category index 7 JSON render as expected.', () => {
    expect(CategoryList[7]).toStrictEqual({"id": "c5", "photoLink": "https://cdn.mos.cms.futurecdn.net/DVgdvxbLT2HDCX7drM38ST-650-80.jpg.webp", "title": "Tablets"});
});

test('Category index 8 JSON render as expected.', () => {
    expect(CategoryList[8]).toStrictEqual({"id": "c8", "photoLink": "https://www.techguide.com.au/wp-content/uploads/2019/06/Pana65OLED2019GZ1500-750x430.jpg", "title": "Televisions"});
});

test('Category index 0 JSON render as expected.', () => {
    expect(CategoryList[9]).toStrictEqual(undefined);
});

