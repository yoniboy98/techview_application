export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
export const SET_FILTERS = 'SET_FILTERS'; 

/**Set an action so the user can filter products. */
export const setFilters = filterSettings => {
    return { type: SET_FILTERS, filters: filterSettings };
};

/**Set an action so the user can toggle between favorite or not on id(product).  */
export const toggleFavorite = (id) => {
    return { type: TOGGLE_FAVORITE, productId: id };
};