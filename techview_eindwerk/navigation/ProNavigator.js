import React from 'react';
import {Text, Platform} from 'react-native';
import {
    createStackNavigator,
    createBottomTabNavigator,
    createDrawerNavigator,
    createAppContainer
} from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {Ionicons} from '@expo/vector-icons';
import CategoriesScreen from '../screens/CategoriesScreen';
import CategoryProductScreen from '../screens/CategoryProductScreen';
import SearchScreen from "../screens/SearchScreen";
import DetailScreen from '../screens/DetailScreen';
import FiltersScreen from '../screens/FiltersScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import CommentScreen from '../screens/CommentScreen';
import SendSmsScreen from '../screens/SendSmsScreen';
import Colors from '../constants/Colors';
import AllProducts from "../screens/AllProducts";
import PopulairProductsScreen from "../screens/PopulairProductsScreen";


/**Create default options for my navigator so it will be looking nice on Ios and android. */
const defaultStackNavOptions = {
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primaryColor : Colors.accentColor
    },
    headerTitleStyle: {
        fontFamily: 'open-sans-bold'
    },
    headerBackTitleStyle: {
        fontFamily: 'open-sans'
    },
    headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primaryColor
};


/**All the stackNavigators that I will use in my application.
 * Create my application title in the header.
 * If the user wants to navigate to product details, the header will change in the name of the product or category.
 * */
const TechNavigator = createStackNavigator({
        Categories: {
            screen: CategoriesScreen,
            navigationOptions: {
                headerTitle: 'Techview'
            }
        },
        Category: CategoryProductScreen,
        Detail: DetailScreen,
        Comments: CommentScreen

    }, {
        defaultNavigationOptions: defaultStackNavOptions
    }
);


const SearchNavigator = createStackNavigator({
        products: {
            screen: SearchScreen,
            navigationOptions: {
                headerTitle: 'Techview'
            }
        },
        Detail: DetailScreen,
        Comments: CommentScreen
    }, {
        defaultNavigationOptions: defaultStackNavOptions
    }
);


const ProNavigator = createStackNavigator({
        products: {
            screen: AllProducts,
            navigationOptions: {
                headerTitle: 'Techview'
            }
        },
        Detail: DetailScreen,
        Comments: CommentScreen


    }, {
        defaultNavigationOptions: defaultStackNavOptions
    }
);


const FavNavigator = createStackNavigator({
        Favorites: FavoritesScreen,
        Detail: DetailScreen,
        Comments: CommentScreen

    }, {
        defaultNavigationOptions: defaultStackNavOptions
    }
);

const popuNavigator = createStackNavigator({
        popular: {
            screen: PopulairProductsScreen,
            navigationOptions: {
                headerTitle: 'popular products'
            }
        },
        Detail: DetailScreen,
        Comments: CommentScreen

    }, {
        defaultNavigationOptions: defaultStackNavOptions
    }
);

const SmsNavigator = createStackNavigator({
        sms: {
            screen: SendSmsScreen,
            navigationOptions: {
                headerTitle: 'Send me something'
            }
        }
    }, {
        defaultNavigationOptions: defaultStackNavOptions
    }
);


const FiltersNavigator = createStackNavigator({
        Filters: FiltersScreen
    }, {
        defaultNavigationOptions: defaultStackNavOptions
    }
);


/**All the BottomNavigators that I will use in my application.
 * I will use icons from ionicon.
 * The colors of the bottom tab will change when you navigate to another page.*/
const tabScreenConfig = {

    Home: {
        screen: TechNavigator,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Ionicons name='ios-home' size={25} color={tabInfo.tintColor}/>
            },
            tabBarColor: Colors.primaryColor,
            tabBarLabel: Platform.OS === 'android'
                ? <Text style={{fontFamily: 'open-sans-bold'}}>Home</Text>
                : 'Home'
        }
    },


    Search: {
        screen: SearchNavigator,

        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Ionicons name="ios-search" size={25} color={tabInfo.tintColor}/>
            },
            tabBarColor: Colors.accentColor,
            tabBarLabel: Platform.OS === 'android'
                ? <Text style={{fontFamily: 'open-sans-bold'}}>Search</Text>
                : 'Search'

        }
    },


    Products: {
        screen: ProNavigator,

        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Ionicons name="ios-list" size={25} color={tabInfo.tintColor}/>
            },
            tabBarColor: Colors.primaryColor,
            tabBarLabel: Platform.OS === 'android'
                ? <Text style={{fontFamily: 'open-sans-bold'}}>Products</Text>
                : 'Products'

        }
    },
};

const ProductsFavTabNavigator = Platform.OS === 'android'
    ? createMaterialBottomTabNavigator(tabScreenConfig, {
        activeTintColor: 'white',
        shifting: true
    })
    : createBottomTabNavigator(tabScreenConfig, {
        tabBarOptions: {
            labelStyle: {
                fontFamily: 'open-sans'
            },
            activeTintColor: Colors.accentColor
        }
    });


/** Navigator where I will store my filter, favorite, popular products and sms options.
 * It will be accessible when the user pressed the optionIcon in the top left.
 * */
const MainNavigator = createDrawerNavigator({
    ProductsFav: {
        screen: ProductsFavTabNavigator,
        navigationOptions: {
            drawerLabel: 'Home'
        }
    },
    Filters: FiltersNavigator,
    Wishlist: FavNavigator,
    Popular: popuNavigator,
    TextMe: SmsNavigator
}, {
    contentOptions: {
        activeTintColor: Colors.accentColor,
        labelStyle: {
            fontFamily: 'open-sans-bold'
        }
    }
});

export default createAppContainer(MainNavigator);