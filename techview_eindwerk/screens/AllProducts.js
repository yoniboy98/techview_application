import React, {useState, useCallback} from 'react'
import {FlatList, RefreshControl} from 'react-native'
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import {useSelector} from "react-redux";
import ProductItemAll from "../components/ProductItemAll";


/** Enable refresh function*/
function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}


/** Render all products that are in my Json file and navigate to the next screen "DetailScreen" on id.
 * Show productName, imageUrl, minimum price, maximum price*/
const AllProducts = props => {
    const renderGridItem = (itemData) => {

        return (
            <ProductItemAll
                productName={itemData.item.productName}
                imageUrl={itemData.item.imageUrl}
                minPrice={itemData.item.minPrice}
                maxPrice={itemData.item.maxPrice}

                onSelectProduct={() => {
                    props.navigation.navigate({
                        routeName: "Detail", params: {
                            productId: itemData.item.id,
                            smallName: itemData.item.smallName,

                        }
                    });
                }}/>
        );
    };


    /** Keeping old values when refreshing.*/
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(() => {
        setRefreshing(true);

        wait(500).then(() => setRefreshing(false));
    }, [refreshing]);


    /**Search for all the products that are available after filtering. */
    const availableProduct = useSelector(state => state.products.filteredProducts);

    /** Show all objects from my Json file and sort it alphabetically
     * set a key to each object so there will be no duplicates
     * Only 1 column*/
    return (
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh}>
            <FlatList style={{backgroundColor: "#dbd9d9"}}
                      numColumns={1}
                      data={availableProduct.sort((a, b) => a.productName.localeCompare(b.productName))}
                      renderItem={(item) => renderGridItem(item)}
                      keyExtractor={(item, index) => index.toString()}/>
        </RefreshControl>
    );

};


/**Set an option Icon in the left top of the header to navigate to other screens. */
AllProducts.navigationOptions = navData => {
    return {
        headerLeft: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName='ios-options'
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        )
    };
};
/**Export my class so it can be called in other classes. */
export default AllProducts
