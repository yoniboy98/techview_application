import React, {useState, useCallback, useEffect} from 'react';
import {
    View,
    Text,
    StyleSheet,
    RefreshControl,
    ScrollView,
    Image,
    TouchableOpacity,
} from 'react-native'
import {findCommentByproductId, getTextDate} from '../func/Aanvragen';

import profilepicture from "../images/profilepicture.png";
import DefaultText from "../components/DefaultText";


/** Enable refresh function*/
function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}


const CommentScreen = props => {
    /** Keeping old values when refreshing.*/
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(() => {
        setRefreshing(true);


        wait(500).then(() => setRefreshing(false));
        }, [refreshing]);


    const [comment, setComment] = useState(null);
    const [dataLoaded, setDataLoaded] = useState(false);

    useEffect(() => {
        (async () => {
            if (!dataLoaded) {
                await loadShowScreen();
            }
        })();
    });

    /**Get the comment on productId from my Api. */
    const productId = props.navigation.getParam('productId');
/**If the length of comments is lower as 1 -> push message
 * If the length is more as 1 -> push commentComponent.
 * Get comment, name, date of creation.
 *  */
    const loadShowScreen = async () => {
        if (comment == null) {
            let comments = await findCommentByproductId(productId);
            console.log(comments);
            let arr = [];
            if (comments.length < 1) {
                arr.push(<DefaultText key={666}>No comments on this product</DefaultText>);
            } else {
                for (const com of comments) {
                    arr.push(
                        <TouchableOpacity key={com.id}>
                            <View style={styles.commentHeader}>
                                <Image source={profilepicture} style={{width: 60, height: 60, borderRadius: 150}}/>
                                <Text>  {com.name} |  {getTextDate(com.commentDate)}</Text>
                            </View>
                            <Text style={styles.textStyle}>
                                {com.comment}
                            </Text>

                        </TouchableOpacity>
                    )
                }
            }
            setDataLoaded(true);
            setComment(arr);
        }

    };

/** Show all comments of the product*/
    return (

        <RefreshControl refreshing={refreshing} onRefresh={onRefresh}>
            <ScrollView keyExtractor={(item, index) => index.toString()}>
                {comment}
            </ScrollView>
        </RefreshControl>
    )
}


CommentScreen.navigationOptions = () => {

    return {
        headerTitle: "Comments"

    }
};

const styles = StyleSheet.create({
    textStyle: {
        borderWidth: 2,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f5de98',
        opacity: 0.7,
        paddingBottom: 16,
        fontFamily: "open-sans",
        paddingLeft: 10
    },
    commentHeader: {
        flexDirection: "row",
        alignItems: "center",
        width: "100%"
    },

});


/**Export my class so it can be called in other classes. */
export default CommentScreen;