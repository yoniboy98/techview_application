import React from 'react';
import {StyleSheet, Text,ScrollView, ImageBackground, Image} from 'react-native';
import {sendSMSAsync} from "expo-sms";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import {Ionicons} from "@expo/vector-icons";
import AwesomeButtonCartman from 'react-native-really-awesome-button/src/themes/cartman';
import Smsbackground from '../images/smsbackground.jpg';


const SendSmsScreen = props => {
    /**Make use of the sms library in expo so I can let the user send me a text message */
    let onPress = async () => {
        await sendSMSAsync(
            '0470565378',
            'Send me something.'
        );
    };
    /**When the user pressed the button, navigate to sms on your phone  */
    return (
        <ImageBackground source={Smsbackground} style={{width: '100%', height: '100%'}}>
            <ScrollView>


                <Text style={{fontFamily: "open-sans-bold", fontSize: 12, marginTop: 35}}>Send me a message when you
                    want a new product
                    in the store.</Text>
                <Image style={styles.gif} source={{uri: "https://media.giphy.com/media/TJmoBmGk4XXkcI6P3t/giphy.gif"}}
                />
                <AwesomeButtonCartman style={styles.btnContainer}
                                      width={100}
                                      onPress={onPress}>


                    <Text> <Ionicons name="ios-send" size={24} color={"blue"}/> SEND </Text></AwesomeButtonCartman>

            </ScrollView>
        </ImageBackground>
    );
};
/**Set an option Icon in the left corner of the header to navigate to other screens. */
SendSmsScreen.navigationOptions = navData => {
    return {
        headerLeft: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName='ios-menu'
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        )
    };
};

const styles = StyleSheet.create({
    btnContainer: {
        marginTop: '10%',
        marginLeft: '37%'
    },
    gif: {
        opacity: 0.4,
        height: 250,
        marginBottom: 20,
        marginTop: 50,
        marginRight: 25


    }
});

/**Export my class so it can be called in other classes. */
export default SendSmsScreen;