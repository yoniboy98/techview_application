import React, {useState} from 'react';
import {ScrollView} from 'react-native'
import SearchItem from "../components/SearchItem";
import {findProductNameLike} from "../func/Aanvragen";
import {findCategoryName} from "../func/Aanvragen";
import ProductItem from "../components/ProductItem";
import CategoryGridTile from "../components/CategoryGridTile";



const Search = props => {

    const [searchedText, setSearchedText] = useState("");
    const [searchInput, setSearchInput] = useState("");
    const [hasSearched, setHasSearched] = useState(false);
    const [results, setResults] = useState([]);

    /** an array to store all found objects in  my Json file PRODUCTS.Json .*/
    let foundResults = [];


    /**Search for the products or categories that match with the user input
     * It is possible to search on everything in the JSon file PRODUCTS and CATEGORY. */
    if (searchedText.length > 1 && hasSearched === false) {
        let foundProducts = findProductNameLike(searchedText), foundCategories = findCategoryName(searchedText);

        /** Loop over all the objects that match the user input.
         * Sett the name and image property .
         * Onclick navigate to Detail.
         * set results
         * Now I can search for products in my Json file.
         * */
        for (let i = 0; i < foundProducts.length; i++) {
            foundResults.push(
                <ProductItem key={i}

                             productName={foundProducts[i].productName}
                             image={foundProducts[i].imageUrl}
                             onSelectProduct={() => {
                                 props.navigation.navigate({
                                     routeName: "Detail", params: {
                                         productId: foundProducts[i].id,
                                         smallName: foundProducts[i].smallName,

                                     }
                                 });
                             }}/>);
        }

        /** Loop over all the objects that match the user input.
         * Sett the title and image photoLink .
         * Onclick navigate to Product.
         * set results
         * now I can search in categories.
         * */
        for (let c = 0; c < foundCategories.length; c++) {
            foundResults.push(
                <CategoryGridTile key={c}

                                  title={foundCategories[c].title}
                                  photoLink={foundCategories[c].photoLink}
                                  onSelect={() => {
                                      props.navigation.navigate({
                                          routeName: "Category", params: {
                                              categoryId: foundCategories[c].id,


                                          }
                                      });
                                  }}/>);
        }

        setResults(foundResults);
        setHasSearched(true);
    }
    return (
        <ScrollView style={{backgroundColor: "#dbd9d9"}}>
            <SearchItem
                onBlur={() => {
                    if (searchInput.length > 1) {
                        setSearchedText(searchInput);
                        setHasSearched(false);
                        setResults([]);

                    }
                }}


                onChangeText={(text) => {
                    if (searchedText.length > 0) {
                        setSearchedText("");

                    }
                    setSearchInput(text);

                }}

            />

            {results}


        </ScrollView>

    )
};


export default Search;
