import React from 'react'
import {FlatList} from 'react-native'
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import PopularProductItems from '../components/PopularProductItems';

/**Call JSON file POPULARPRODUCTS */
const PopularProducts = require('../assets/POPULARPRODUCTS');


/** Render all products that are in my Json file and navigate to the next screen "DetailScreen".
 * Show productName and imageUrl*/
const PopulairProductsScreen = props => {
    const renderGridItem = (itemData) => {

        return (

            <PopularProductItems
                productName={itemData.item.productName}
                imageUrl={itemData.item.imageUrl}

                onSelectProduct={() => {
                    props.navigation.navigate({
                        routeName: "Detail", params: {
                            productId: itemData.item.id,
                            smallName: itemData.item.smallName,

                        }
                    });
                }}/>
        );
    }


    /** Show all objects from my Json file.
     * set a key to each object so there will be no duplicates*/
    return (

        <FlatList
            style={{backgroundColor: "#dbd9d9"}}
            numColumns={1}
            data={PopularProducts}
            renderItem={(item) => renderGridItem(item)}
            keyExtractor={(item, index) => index.toString()}
        />


    );

};


/**Set an option Icon in the left corner of the header to navigate to other screens. */
PopulairProductsScreen.navigationOptions = navData => {
    return {
        headerLeft: (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName='ios-menu'
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        )
    };
};

/**Export my class so it can be called in other classes. */
export default PopulairProductsScreen;
