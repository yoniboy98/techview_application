import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';

import ProductList from '../components/ProductList';
import DefaultText from '../components/DefaultText';


/** Calling Json file CATEGORY.json.*/
let CATEGORY = require('../assets/CATEGORY');


const CategoryProductScreen = props => {
    /**Get the categoryId */
    const categoryId = props.navigation.getParam('categoryId');

    /**Search for available products */
    const availableProduct = useSelector(state => state.products.filteredProducts);


    /**Filter all products that are available */
    const displayedProducts = availableProduct.filter(
        product => product.categoryIds.indexOf(categoryId) >= 0
    );

    /**If no products are found the DefaultText with a message will be displayed. */
    if (displayedProducts.length === 0) {
        return (
            <View style={styles.content}>
                <DefaultText>No products found, maybe check your filters?</DefaultText>
            </View>
        );
    }
/**Returns all the available products in a list. */
    return <ProductList listData={displayedProducts} navigation={props.navigation}/>
};

/**Set product on id in the header and display the category title. */
CategoryProductScreen.navigationOptions = navigationData => {
    const categoryId = navigationData.navigation.getParam('categoryId');
    const selectedCategory = CATEGORY.find(category => category.id === categoryId);

    return {
        headerTitle: selectedCategory.title
    };
};

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

/**Export my class so it can be called in other classes. */
export default CategoryProductScreen;